import axios from "../axios/axios";

const removerCategoriaServico = async (id) => {
    try{
        await axios.delete(`categoria/${id}`);
        return {
            'sucesso':false,
            'mensagem': 'Categoria removida com sucesso.'
        };
    }catch(error){


        switch(error.response.status){
            case 409:
                return {
                    'sucesso':false,
                    'mensagem': error.response.data.message
                };
            default:
                return {
                    'sucesso':false,
                    'mensagem': 'Houve algumo problema.'
                };
        };
    };
};

export default removerCategoriaServico;