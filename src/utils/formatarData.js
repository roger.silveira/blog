const formatarData = (sData) =>{
    const sAno = sData.substring(0,4);
    const sMes = sData.substring(5,7);
    const sDia = sData.substring(8,10);
    return sDia + '/' + sMes + '/' + sAno;    
};

export default formatarData;