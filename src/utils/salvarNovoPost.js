import axios from '../axios/axios.js';

const salvarNovoPost = async (novoPost, setMensagem) => {
    try{
        await axios.post('post', novoPost);
        setMensagem('O novo post foi cadastrado.');        
    }catch(error){
        setMensagem(error.message);
        return;
        // switch(error.response.status){
        //     case 409:
        //         setMensagem(error.response.data.message); 
        //         break;
        //     default:
        //         setMensagem('Houve algum problema no cadastro do novo post.');
        //         break;
        // };
    };
    
};

export default salvarNovoPost;