const COR_1 = 'branco';
const COR_2 = 'rosa';
const COR_PADRAO = 'azul';

const COR_1_CONFIGURACOES = {
    corFundoTema: 'lightpink',
    corFundoBotaoContinueLendo: 'hotpink',
    corFundotextContinueLendo: 'white',
    corFonte: 'black',
};

const COR_2_CONFIGURACOES = {
    corFundoTema: 'black',
    corFundoBotaoContinueLendo: 'hotpink',
    corFundotextContinueLendo: 'white',
    corFonte: 'white',
};

const COR_PADRAO_CONFIGURACOES = {
    corFundoTema: 'beige',
    corFundoBotaoContinueLendo: 'hotpink',
    corFundotextContinueLendo: 'white',
    corFonte: 'black',
};

export {COR_1,
    COR_2,
    COR_PADRAO
};

export {COR_1_CONFIGURACOES,
    COR_2_CONFIGURACOES,
    COR_PADRAO_CONFIGURACOES
};