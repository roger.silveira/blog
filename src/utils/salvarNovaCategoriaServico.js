import axios from '../axios/axios.js';

const salvarNovaCategoriaServico = async (novaCategoria, setMensagem) => {
    try{
        await axios.post('categoria', novaCategoria);
        setMensagem('A nova categoria foi cadastrada.');        
    }catch(error){
        setMensagem(error.message);
        return;        
        // switch(error.response.status){
        //     case 409:
        //         setMensagem(error.response.data.message); 
        //         break;
        //     default:
        //         setMensagem('Houve algum problema no cadastro do novo post.');
        //         break;
        // };
    };
    
};

export default salvarNovaCategoriaServico;