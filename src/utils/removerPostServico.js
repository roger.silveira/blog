import axios from '../axios/axios.js';

const removerPostServico = async (idPost, setMensagem) => {
    console.log(`idPost: ${idPost}`);

    try{
        // const resposta = await axios.delete(`post/${idPost}`);
        await axios.delete(`post/${idPost}`);

        setMensagem('O post foi removido.');    

    }catch(error){
        setMensagem(error.message);
        return;
        // switch(error.response.status){
        //     case 409:
        //         return {
        //             'sucesso':false,
        //             'mensagem': error.response.data.message
        //         };
        //     default:
        //         return {
        //             'sucesso':false,
        //             'mensagem': 'Houve algumo problema.'
        //         };
        // };
    };
};

export default removerPostServico;