import axios from '../axios/axios.js';
const _listaPosts = [
    {
        'id':1,
        'idCategoria': 1,
        'titulo': 'Conjunto de Melancia 🍉',
        'descricao': 'Sutiã Angélica, calcinha Dayana, esse biquíni é perfeito para os dias de sol, confortável e lindo!',
        'dataPostagem': '2021-05-27',
        'imagemCaminho': './imagens/conjunto-melancia.jpg',
        'imagemAlt': 'Conjunto melancia'            
    },
    {
        'id':2,
        'idCategoria': 2,
        'titulo': 'Tal mãe, tal filha! ',
        'descricao': 'Você combinando com a sua filha 🤩 ✨Maiô Fernanda, tamanho do PP ao G4. ✨Maiô infantil.',
        'dataPostagem': '2021-05-29',
        'imagemCaminho': './imagens/tal-mae-tal-filha.jpg',
        'imagemAlt': 'Conjunto tal mãe tal filha'            
    }
];

const pegarPosts = async (salvarState) => {

    // _listaPosts.sort((a,b) => {
    //     return (a.dataPostagem < b.dataPostagem) ? 1 : -1;
    //  });

    // salvarState(_listaPosts)

    try{
        const resposta = await axios.get('post');
        salvarState(resposta.data);
    }catch(erro){
        console.log(`Ocorreu um erro: ${erro.message}.`);
    };   

};

export const pegarPostsPorID = async (id, setPostDetalhado) => {

    const resposta = await axios.get(`post/${id}`);
    setPostDetalhado(resposta.data);
    
    // const _post = _listaPosts.find(post => {
    //     let nId = (id);
    //     return post.id === nId;
    // });
    // return _post;
};

export default pegarPosts;
