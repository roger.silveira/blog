import axios from '../axios/axios.js';

const pegarCategorias = async (salvarState) => {
        
    // const _listaCategorias = [
    //     {
    //         'id':1,
    //         'descricao': 'Biquinis'
    //     },
    //     {
    //         'id':2,
    //         'descricao': 'Kits Família'
    //     }
    // ];

    // const _listaCategoriasOrdenada = _listaCategorias.sort();
    // _listaCategorias.sort((a,b) => {
    //     return (a.descricao > b.descricao) ? 1 : -1;
    //  });

    // salvarState(_listaCategorias);

    try{
        const resposta = await axios.get('categoria');
        salvarState(resposta.data);
    }catch(erro){
        console.log(`Ocorreu um erro: ${erro.message}.`);
    };   


};

export const pegarCategoriasPorID = async (idCategoria) => {
    console.log(`idPost: ${idCategoria}`);

    try{
        // const resposta = await axios.delete(`post/${idPost}`);
        const resposta = await axios.get(`categoria/${idCategoria}`);
        return resposta.data.categoria;
        // return {            
        //     'sucesso':true,
        //     'mensagem': 'Post removido.'
        // };
    }catch(erro){
        console.log(`Ocorreu um erro: ${erro.message}.`);
    };   
};

export default pegarCategorias;