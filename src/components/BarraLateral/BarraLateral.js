import React, {useEffect, useState} from 'react';

import  './BarraLateral.css'
import Busca from './Busca/Busca.js'
import ListaCategorias from './ListaCategorias/ListaCategorias.js'

import pegarCategorias from '../../utils/pegarCategorias.js'


const BarraLateral = () => {

    const [categorias, setCategorias] = useState([]);

    useEffect(() => {
        pegarCategorias(setCategorias);
    }, []);

    return (
        <aside>
            {/* <h1>BarraLateral</h1> */}
            <Busca />
            <ListaCategorias lista={categorias}/>
        </aside>
    );
};

export default BarraLateral;