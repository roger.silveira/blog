import React, {useState} from 'react';
import {BrowserRouter} from 'react-router-dom';

import  './PaginaInicial.css'

import Cabecalho from '../Cabecalho/Cabecalho.js'
import ConteudoCentral from '../ConteudoCentral/ConteudoCentral.js'
import BarraLateral from '../BarraLateral/BarraLateral.js'
import Rodape from '../Rodape/Rodape.js'

import TemaContext from '../../contexts/TemaContext.js'
import {COR_PADRAO, COR_1, COR_2} from '../../utils/coresTema.js'
import {COR_PADRAO_CONFIGURACOES, COR_1_CONFIGURACOES, COR_2_CONFIGURACOES} from '../../utils/coresTema.js'

const PaginaInicial = () => {

    const [tema, setTema] = useState(COR_PADRAO_CONFIGURACOES);

    const modificarTema = (temaSelecionado) =>{
        switch(temaSelecionado) {
            case COR_1:
                setTema(COR_1_CONFIGURACOES);            
                break;
            case COR_2:
                setTema(COR_2_CONFIGURACOES);            
                break;
            case COR_PADRAO:
                setTema(COR_PADRAO_CONFIGURACOES);
                break;
            default:
                setTema(COR_PADRAO_CONFIGURACOES);
                break;
        };
    };
    
    return (

        <TemaContext.Provider value={tema}>
            <BrowserRouter>

                <div id='box-pagina-inicial'>

                    <Cabecalho funcaoConfiguraTema={modificarTema} />

                    <div id='container'>
                        <ConteudoCentral />
                        <BarraLateral />
                    </div>

                    <Rodape />

                </div>
                
            </BrowserRouter>
        </TemaContext.Provider>
    );
};

export default PaginaInicial;