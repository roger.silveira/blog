import React from 'react';

import  './Rodape.css'
const Rodape = () => {
    return (
        <footer>
            Copyright &#169; - Infnet
        </footer>
    );
};

export default Rodape;