import React, {useState} from 'react';
import {Link} from 'react-router-dom';

import  './ListaPosts.css';

import removerPostServico from '../../../utils/removerPostServico.js';

const ListaPosts = ({lista}) => {
    

    const [mensagem, setMensagem] = useState('');
    const removerPost = async(item) => {
        let resposta = window.confirm("Você tem certeza que deseja excluir o post?");
        //const resposta = true;
        if (resposta){
            await removerPostServico(item.id, setMensagem);        
        };  
    };

    return (
        
        <div id='lista-posts'>
            
            <div id='lp-box-titulo-btn'>
                <h2 id='lp-titulo'>Lista de Posts</h2>            
                {mensagem? <p id='np-mensagem'>{mensagem}</p>:null}
                <Link to='/novo-post'>
                    <button id='lp-btn-novo-post'>Novo Post</button>
                </Link>
            </div>

            <ul id='lp-posts'>
                {lista ? lista.map(item => {
                    return <li className='lp-item' key={item.id}>
                        {item.titulo}
                        <button className='lp-item-btn-remover'
                            onClick={() => {removerPost(item)}}
                        >Remover</button>                        
                    </li>
                    }) 
                    : <p>Carregando posts...</p>
                }
            </ul>

        </div>
    );
};

export default ListaPosts;