// import React, {useEffect, useState} from 'react';
import React, {useContext, useEffect, useState} from 'react';
import {Link, useParams} from 'react-router-dom';

import  './DetalhesPost.css'

import pegarCategorias from '../../../utils/pegarCategorias.js'
import {pegarPostsPorID} from '../../../utils/pegarPosts.js'

import TemaContext from '../../../contexts/TemaContext';

import formatarData from '../../../utils/formatarData';

const DetalhesPost = () => {

    const {id} = useParams();
    const [categorias, setCategorias] = useState([]);
    const [postDetalhado, setPostDetalhado] = useState({});

    useEffect(() => {
        pegarCategorias(setCategorias);              
        pegarPostsPorID(id, setPostDetalhado);
    }, [id]);

    const obterNomeCategoria = (id) =>{
        if(categorias && categorias.length > 0) {
            const _categorias = categorias.filter(categoria => {
                return categoria.id === (id);
            });
            if(_categorias.length === 1){
                //Pegando a descricao do primeiro (e unico) item retornado.
                return _categorias[0].categoria;
            };
        };
    };

    // return (
    //     <>
    //     {postDetalhado?
    //     <div id='cc-detalhes-post'>
    //         <h2>Detalhes Post</h2>  
    //         <p>Id Selecionado: {id}</p>
    //         <p>Descricao: {postDetalhado? postDetalhado.descricao : null}</p>
    //         <p>Categoria: {postDetalhado? obterNomeCategoria(postDetalhado.idCategoria) : null}</p>
                        
            


    //     </div>
    //     : null
    //     }
    //     </>
    // );

    const tema = useContext(TemaContext);
    const corFundoTema = tema.corFundoTema;
    const corFonte = tema.corFonte;

    return (        
        <>
        {postDetalhado?
        <article className='detalhes-post' style={{backgroundColor:corFundoTema}}>            
            
            <div className='p-boxe-titulo=btn-remover'>
                <h2 className='detalhes-p-titulo' style={{color:corFonte}}>{postDetalhado.titulo}</h2>

                {/* <button className='p-btn-remover' onClick={() =>removerPost()}>Remover</button> */}
            </div>
            <p className='detalhes-p-postado-em' style={{color:corFonte}}> Postado em:  {postDetalhado.dataPostagem? 
                formatarData(postDetalhado.dataPostagem) + " ": " "} 
                    na categoria 
                    <Link to={`/posts-por-categoria/${postDetalhado.idCategoria}`}>
                        {" " + obterNomeCategoria(postDetalhado.idCategoria) + " "}
                    </Link>                           </p>

            <div className='detalhes-p-img-texto'>
                <div className='detalhes-p-img' >
                    <img  src={postDetalhado.imagemCaminho} alt={postDetalhado.imagemAlt}/>
                </div>
                
                <div className='detalhes-p-texto'>
                    <p style={{color:corFonte}}>{postDetalhado.descricao}</p>                    
                </div>
            </div>

             
            {/* <div className='p-categ-bt'>
                <div className='p-categoria' style={{color:corFonte}}>                    
                    Categoria: 
                    <Link to={`/posts-por-categoria/${postDetalhado.idCategoria}`}>
                        {obterNomeCategoria(postDetalhado.idCategoria)}
                    </Link>                           
                </div>
            </div> */}

        </article>    
        : null
        }
        </>
    );
};

export default DetalhesPost;