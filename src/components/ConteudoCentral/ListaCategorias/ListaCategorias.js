import React, {useState} from 'react';
import {Link} from 'react-router-dom';

import  './ListaCategorias.css';

import removerCategoriaServico from '../../../utils/removerCategoriaServico';



const ListaCategorias = ({lista}) => {

    const [categoriaRemovida, setCategoriaRemovida] = useState(false);
    const removerCategoria = async(item) => {
         const resposta = window.confirm("Você tem certeza que deseja excluir a categoria?");
        
        if (resposta){
            const retornoServico = await removerCategoriaServico(item.id);        
            if (retornoServico.sucesso) {
                setCategoriaRemovida(true);
                return false;
            };
        };  
    };

    return (
        
        <div id='lista-categorias'>

            <div id='lc-box-titulo-btn'>
                <h2 id='lc-titulo'>Lista de Categorias</h2>            
                <Link to='/nova-categoria'>
                    <button id='lc-btn-nova-categoria'>Nova Categoria</button>
                </Link>
            </div>

            <ul id='lc-categorias'>
                {lista ? lista.map(item => {
                    return <li className='lc-item' key={item.id}>
                        {item.categoria}
                        <button className='lc-item-btn-remover'
                            onClick={() => {removerCategoria(item)}}
                        >Remover</button>
                            
                        {/* <Link to={`posts-por-categoria/${item.id}`} className='lc-item-btn-listar-posts'>
                            Listar Posts
                        </Link> */}
                    </li>
                    }) 
                    : <p>Carregando categorias...</p>
                }
            </ul>

        </div>
    );
};

export default ListaCategorias;