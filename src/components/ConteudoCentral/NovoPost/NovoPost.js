import React, {useState} from 'react';

import  './NovoPost.css';

import salvarNovoPost from '../../../utils/salvarNovoPost';

const NovoPost = ({categorias}) => {

                      
    const [titulo, setTitulo] = useState('');
    const [categoria, setCategoria] = useState('');
    const [descricao, setDescricao] = useState('');
    const [imagem, setImagem] = useState('');
    const [imagemAlt, setImagemAlt] = useState('');

    const [mensagem, setMensagem] = useState('');

    const incluirNovoPost = evento => {

        // console.log('Form \'submittado\'');
        evento.preventDefault();

        if(!titulo || !categoria || !descricao || !imagem){
            alert('Preencha todos os campos.');
            return false;
        };

        const hoje = new Date();

        const ano = hoje.getFullYear();
        const mes = hoje.getMonth();
        const dia = hoje.getDate();

        const mesFormatado = ((mes + 1) < 10 ? 
            "0" + String((mes + 1)) 
            : String(mes + 1)
        );

        const novoPost = {
            'idCategoria': categoria,
            'titulo': titulo,            
            'descricao': descricao,
            'dataPostagem': `${ano}-${mesFormatado}-${dia}`,
            'imagemCaminho': imagem,            
            'imagemAlt': imagemAlt
        };

        salvarNovoPost(novoPost, setMensagem);

    };

    return (
        <div>
            <div id= 'np'>
                <h4>Novo Post</h4>

                {mensagem? <p id='np-mensagem'>{mensagem}</p>:null}

                <form onSubmit={evento => incluirNovoPost(evento)}>

                    <div className='np-campo'>
                        <label htmlFor='np-campo-titulo'>Título: </label>
                        <input 
                            id='np-campo-titulo' 
                            name='np-campo-titulo' 
                            value={titulo} 
                            onChange={evento => setTitulo(evento.target.value)}
                        />
                    </div>                        
                    <div className='np-campo'>
                        <label htmlFor='np-campo-categoria'>Categoria: </label>
                        <select 
                            id='np-campo-categoria' 
                            name='np-campo-categoria' 
                            value={categoria} 
                            onChange={evento => setCategoria(evento.target.value)}>
                                <option value={-1} disabled>Selecione uma categoria...</option>
                                
                                {categorias.map(item => {
                                    return <option 
                                        value={item.id} 
                                        key={item.id}>
                                            {item.categoria}
                                    </option>
                                })}
                        </select> 
                    </div>
                    <div className='np-campo'>
                        <label htmlFor='np-campo-descricao'>Descrição: </label>
                        <textarea 
                            id='np-campo-descricao' 
                            name='np-campo-descricao' 
                            value={descricao} 
                            onChange={evento => setDescricao(evento.target.value)}                            
                        />
                    </div>
                    <div className='np-campo'>
                        <label htmlFor='np-campo-imagem'>Caminho da Imagem: </label>
                        <input                            
                            id='np-campo-imagem' name='np-campo-imagem' value={imagem} onChange={evento => setImagem(evento.target.value)}
                        />
                    </div>
                    <div className='np-campo'>
                        <label htmlFor='np-campo-imagem-alt'>Descrição da Imagem: </label>
                        <input                            
                            id='np-campo-imagem-alt' name='np-campo-imagem-alt' value={imagemAlt} onChange={evento => setImagemAlt(evento.target.value)}
                        />
                    <div className='np-campo'></div>                        
                        <button id='np-bt-salvar'>Salvar</button>
                    </div>
                                     
                                  
                </form>
            </div>
        </div>
    );
};

export default NovoPost;