import React, {useEffect, useState}  from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import  './ConteudoCentral.css'

import ExibirPosts from './ExibirPosts/ExibirPosts.js'
import ListaCategorias from './ListaCategorias/ListaCategorias.js'
import ListaPosts from './ListaPosts/ListaPosts.js'
import NovoPost from './NovoPost/NovoPost.js'
import DetalhesPost from './DetalhesPost/DetalhesPost.js'
import PostsPorCategoria from './PostsPorCategoria/PostsPorCategoria.js'
import NovaCategoria from './NovaCategoria/NovaCategoria.js'

import pegarCategorias from '../../utils/pegarCategorias.js'
import pegarPosts from '../../utils/pegarPosts.js'

const ConteudoCentral = () => {

    const [categorias, setCategorias] = useState([]);
    const [posts, setPosts] = useState([]);

    useEffect(() => {
        pegarCategorias(setCategorias);
    }, []);
    useEffect(() => {
        pegarPosts(setPosts);
    }, []);

    return (
        <main> 

            <Switch>

                <Route exact path="/"> 
                    <Redirect to='exibir-posts' /> 
                </Route>                
                <Route path="/exibir-posts"> 
                    <ExibirPosts lista={posts}/> 
                </Route>
                <Route path="/lista-categorias"> 
                    <ListaCategorias lista={categorias} /> 
                </Route>
                <Route path="/lista-posts"> 
                    <ListaPosts lista={posts} /> 
                </Route>
                <Route path="/novo-post"> 
                    <NovoPost categorias={categorias}/> 
                </Route>

                <Route path="/detalhes-post/:id" component={DetalhesPost} /> 
                <Route path="/posts-por-categoria/:idCategoria" component={PostsPorCategoria} /> 
                <Route path="/nova-categoria" component={NovaCategoria} /> 

            </Switch>

        </main>
    );
};

export default ConteudoCentral;