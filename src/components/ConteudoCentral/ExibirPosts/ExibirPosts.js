import React from 'react';

import  './ExibirPosts.css'

import Post from './Post/Post.js'

const ExibirPosts = ({lista}) => {
    return (
        <div className='exibir-posts'>            

            {lista && lista.length > 0 ?
                lista.map(item => <Post post={item}/>)
            : <p>Carregando...</p>
            }
                        
        </div>
    );
};

export default ExibirPosts;