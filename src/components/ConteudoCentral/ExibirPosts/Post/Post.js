import React, {useContext, useEffect, useState} from 'react';
import {Link} from 'react-router-dom';

import TemaContext from '../../../../contexts/TemaContext.js'

import pegarCategorias from '../../../../utils/pegarCategorias.js'

import formatarData from '../../../../utils/formatarData.js'

import removerPostServico from '../../../../utils/removerPostServico.js'

import  './Post.css'

const Post = ({post}) => {
    
    const [categorias, setCategorias] = useState([]);

     const obterNomeCategoria = (id) =>{
        if(categorias && categorias.length > 0) {
            const _categorias = categorias.filter(categoria => {
                return categoria.id === (id);
            });
            if(_categorias.length === 1){
                //Pegando a descricao do primeiro (e unico) item retornado.
                return _categorias[0].categoria;
            };
        };
    };

    useEffect(() => {
        pegarCategorias(setCategorias);
    }, []);


    const [postRemovido, setpostRemovido] = useState(false);

    const removerPost = async () =>{
        const resultado = await removerPostServico(post.id);
        if(resultado.sucesso){
            alert(resultado.mensagem);
            setpostRemovido(true);
            return false;
        }
        alert(resultado.mensagem);
    };

    const tema = useContext(TemaContext);
    const corFundoTema = tema.corFundoTema;
    const corFundoBotaoContinueLendo = tema.corFundoBotaoContinueLendo;
    const corFundotextContinueLendo = tema.corFundotextContinueLendo;
    const corFonte = tema.corFonte;

    return (
        <>
        {postRemovido? 
            null :

            <article className='post' style={{backgroundColor:corFundoTema}}>            
                
                <div className='p-boxe-titulo=btn-remover'>
                    <h3 className='p-titulo' style={{color:corFonte}}>{post.titulo}</h3>

                    {/* <button className='p-btn-remover' onClick={() =>removerPost()}>Remover</button> */}
                </div>
                <p className='p-postado-em' style={{color:corFonte}}> Postado em:  {formatarData(post.dataPostagem)}</p>

                <div className='p-img-texto'>
                    <div className='p-img' >
                        <img  src={post.imagemCaminho} alt={post.imagemAlt}/>
                    </div>
                    
                    <div className='p-texto'>
                        <p style={{color:corFonte}}>{post.descricao}</p>                    
                    </div>
                </div>

                <div className='p-categ-bt'>
                    <div className='p-categoria' style={{color:corFonte}}>
                        {/* Categoria: {post.idCategoria} */}
                        Categoria: 
                        <Link to={`/posts-por-categoria/${post.idCategoria}`}>
                            {obterNomeCategoria(post.idCategoria)}
                        </Link>                           
                    </div>
                    <Link 
                        to={`/detalhes-post/${post.id}`}
                        className='p-botao-continue-lendo' 
                        style={
                            {backgroundColor:corFundoBotaoContinueLendo, 
                            color:corFundotextContinueLendo}
                        }>
                            Continue lendo...
                    </Link>
                </div>
                
            </article>
        }
        </>
    );
};

export default Post; 