 import React, {useEffect, useState} from 'react';

 import  './PostsPorCategoria.css'

import {useParams} from 'react-router-dom';

import Post from '../ExibirPosts/Post/Post.js'

import pegarPosts from '../../../utils/pegarPosts.js'

const PostsPorCategoria = () => {
    const {idCategoria} = useParams();

    const [posts, setPosts] = useState([]);
    const [postsFiltrados, setPostsFiltrados] = useState([]);  

    useEffect(()=>{
        pegarPosts(setPosts);
    }, []);

    useEffect(()=>{
        if(posts && posts.length > 0) {
            const _postsFiltrados = posts.filter(post => {
                return post.idCategoria === (idCategoria);
            });
            setPostsFiltrados(_postsFiltrados);
        }
    },[posts, idCategoria]);

    return (
        <div id='cc-post-por-categoria'>
            <h2>Posts por Categoria</h2>  
            <p>Id Selecionado: {idCategoria}</p>

            <div>
                {postsFiltrados.map(post =>{
                    // return <p>{post.id} - {post.idCategoria} - {post.descricao}</p>
                    return <Post post={post}/>
                })}


            {/* {lista && lista.length > 0 ?
                lista.map(item => <Post post={item}/>)
            : <p>Carregando...</p>
            } */}

            </div>

        </div>
    );
};

export default PostsPorCategoria;