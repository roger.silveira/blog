import React, {useState} from 'react';
import  './NovaCategoria.css'
import salvarNovaCategoriaServico from  '../../../utils/salvarNovaCategoriaServico.js';

const NovaCategoria = ({post}) => {
    
    const [categoria, setCategoria] = useState('');

    const [mensagem, setMensagem] = useState('');


    const salvarNovaCategoria = evento => {
        evento.preventDefault();
        if(!categoria){
            alert('Preencha o nome da categoria');
            return false;
        };

        const novaCategoria = {
            "categoria": categoria
        }

        salvarNovaCategoriaServico(novaCategoria, setMensagem);

    };

    return (
        <div id='nc'>
            <h4>Nova Categoria</h4>

            {mensagem? <p id='nc-mensagem'>{mensagem}</p>:null}

            <form onSubmit={evento => salvarNovaCategoria(evento)}>
                <div className='nc-campo'>
                <label>Nome da Categoria:</label>
                <input
                id='nc-categoria'
                name='nc-categoria'
                onChange={evento => setCategoria(evento.target.value)}>

                </input>
                </div>
                <button id='nc-btn-salvar'>Salvar</button>
            </form>

        </div>
    );
};

export default NovaCategoria; 