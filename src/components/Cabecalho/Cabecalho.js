import React, {useContext} from 'react';

import  './Cabecalho.css'
import MenuPrincipal from './MenuPrincipal/MenuPrincipal.js'

import TemaContext from '../../contexts/TemaContext.js'

import {COR_PADRAO,COR_1,COR_2} from '../../utils/coresTema.js'

const Cabecalho = ({funcaoConfiguraTema}) => {

    const tema = useContext(TemaContext);
    const corFundoTema = tema.corFundoTema;
    const corFonte = tema.corFonte;

    return (
        <header>
            
            <div id='c-engloba-temas-menu' style={{backgroundColor:corFundoTema}}>

                <MenuPrincipal />
                
                <div id='c-titulo-temas'>
                {/* <h1>Blog</h1> */}
                    <div id='c-temas'>
                        <p style={{color:corFonte}}>Temas:</p>
                        <button id='c-tema-beige'onClick={()=>{funcaoConfiguraTema(COR_PADRAO)}}>
                            Delicada
                        </button>
                        <button id='c-tema-rosa' onClick={()=>{funcaoConfiguraTema(COR_1)}}>
                            Poderosa
                        </button>
                        <button id='c-tema-escuro'onClick={()=>{funcaoConfiguraTema(COR_2)}}>
                            Tema Escuro
                        </button>
                    </div>
                </div>
            

                
            </div>
        </header>
    );
};

export default Cabecalho;