import React, {useContext} from 'react';
import {Link} from 'react-router-dom';

import TemaContext from '../../../contexts/TemaContext.js'

import  './MenuPrincipal.css'
const MenuPrincipal = () => {

    const tema = useContext(TemaContext);
    const corFundoTema = tema.corFundoTema;
    const corFonte = tema.corFonte;

    return (
        <nav style={{backgroundColor:corFundoTema}}>
            <Link to='/exibir-posts'>  
                <div className='c-img'>
                    <img  src='imagens/minas.jpg' alt='logo minas' />                            
                </div>                        
            </Link>
            <ul>
                <li>                    
                    <Link to='/exibir-posts' style={{color:corFonte}}> <h1 style={{color:corFonte}}>Minas na Areia</h1> </Link>
                </li>                
                <li>                    
                    <Link to='/lista-posts' style={{color:corFonte}}> Posts </Link>
                </li>
                <li>                    
                    <Link to='/lista-categorias' style={{color:corFonte}}> Categorias </Link>
                </li>
                {/* <li>
                    <Link to='/novo-post' style={{color:corFonte}}> Novo Post </Link>
                </li> */}
            </ul>
        </nav>
    );
};

export default MenuPrincipal;